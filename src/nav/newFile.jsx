useEffect(() => {
  document.getElementById("ElementID").scrollIntoView();
  // or, use a ref and
  elementRef.current.scrollIntoView();
}, []);
