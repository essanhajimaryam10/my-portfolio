import './Home.css'
import image from '../image.png';
import Skils from "../skils/Skils";
import About from "../about/About";
import Contact from "../contacts/Contact";
import Portfolio from "../portfolio/Portfolio";

function Home() {
    return (
      <>
      <div className="main" id="main">
        <div className="left">
            <h5>HELLO, THERE 👋</h5>
            <h3>I am Maryam, <span> Web App Dev. </span>based in Morocco.</h3>
            {/* <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum
                tristique.
            </p> */}



            <button>View My Work</button>
        </div>
        <div className="right">
            <img src={image}/>
            </div>
    </div>
    <hr />

    <Skils/>
    <hr />
    <About/>
    <hr />

    <Portfolio/>
    <hr />

    <Contact/>

      </>
    );
  }
  
  export default Home;
  