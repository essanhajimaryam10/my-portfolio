import AboutMe from "./about/About";
import Contact from "./contacts/Contact";
import Home from "./home/Home";
import Nav from "./nav/Nav";
import { BrowserRouter , Routes, Route } from 'react-router-dom';
import Skils from "./skils/Skils";
import Portfolio from "./portfolio/Portfolio";


function App() {
  return (
    <>
      <Nav/>
    <BrowserRouter>
     <Routes>
      
      
          <Route path="/" exact element={<Home/>} />
          <Route path="/skils" element={<Skils/>} />
          <Route path="/aboutMe" element={<AboutMe/>} />
          <Route path="/portfolio" element={<Portfolio/>} />
          <Route path="/contact" element={<Contact/>} />


      
    </Routes>
    </BrowserRouter>
    </>
  );
}

export default App;
