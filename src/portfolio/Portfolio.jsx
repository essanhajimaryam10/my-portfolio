import { Link } from "react-router-dom"
import "./Portfolio.css"
import image from"./imageMusuc.PNG"
import image1 from"./image1.PNG"
import image2 from"./image2.PNG"
function Portfolio() {
    return (
      <>
      <h1 class="section-header">Portfolio</h1>
    <div class="container">
      
  
         <div class="card">
            <div class="img">
               <img src={image}/>
            </div>
            <div class="top-text">
               <div class="name">
                  Annie Lea
               </div>
               <p>
                  Apps Developer
               </p>
            </div>
            <div class="bottom-text">
               <div class="text">
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem quaerat iusto adipisci reprehenderit quasi cum perspiciatis, minima reiciendis magni quam!
               </div>
               <div class="btn">
                  <Link href="https://images.pexels.com/photos/19986476/pexels-photo-19986476/free-photo-of-bois-sale-building-mur.jpeg?auto=compress&cs=tinysrgb&w=600&lazy=load">Read more</Link>
               </div>
            </div>
         </div>
         <div class="card">
            <div class="img">
               <img src={image1}/>
            </div>
            <div class="top-text">
               <div class="name">
                  Eliana Maia
               </div>
               <p>
                  Website Developer
               </p>
            </div>
            <div class="bottom-text">
               <div class="text">
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem quaerat iusto adipisci reprehenderit quasi cum perspiciatis, minima reiciendis magni quam!
               </div>
               <div class="btn">
                  <Link href="https://images.pexels.com/photos/19986476/pexels-photo-19986476/free-photo-of-bois-sale-building-mur.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2">Read more</Link>
               </div>
            </div>
         </div>
         <div class="card">
            <div class="img">
               <img src={image2}/>
            </div>
            <div class="top-text">
               <div class="name">
                  Harley Briana
               </div>
               <p>
                  Graphic Developer
               </p>
            </div>
            <div class="bottom-text">
               <div class="text">
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem quaerat iusto adipisci reprehenderit quasi cum perspiciatis, minima reiciendis magni quam!
               </div>
               <div class="btn">
                  <Link href="https://images.pexels.com/photos/19986476/pexels-photo-19986476/free-photo-of-bois-sale-building-mur.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2">Read more </Link>
               </div>
            </div>
         </div>
         </div>

         </>
      
      
    );
  }
  
  export default Portfolio;